import _ from 'lodash';
import Vue from 'vue';

Vue.filter('lodashGet', function(object: any, path: string, value: any = '') {
  return _.get(object, path, value);
});
