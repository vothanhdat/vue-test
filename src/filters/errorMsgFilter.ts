import Vue from 'vue';

const defaultContext = { $t: e => e }

Vue.filter('errorMsg', function (value: any, vueContext: any = {}, path = '') {
  vueContext = vueContext || defaultContext
  var data: any = undefined
  if (path && value && value.response && value.response.data instanceof Object) {
    data = value.response.data as Object;
  }
  if (value.type) {
    data = value as Object;
    if (data.field) {
      data = {
        ...data,
        field: vueContext.$t(path + '.' + data.field)
      }
    }
  }

  if (typeof data == 'object' && (data.error || data.type)) {
    var key = `${path}.${data.error || data.type}`
    var error = vueContext.$t(key, data)
    return error == key
      ? data.error_description
      : error;
  }

  if (path && value) {
    return vueContext.$t(`${path}.${value}`)
  }

  return (value || "").toString()
});
