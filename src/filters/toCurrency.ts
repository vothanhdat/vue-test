import Vue from 'vue';

Vue.filter('toCurrency', function (text: string | number, currency = 'USD') {
  text = typeof text == 'number' ? text : parseFloat(text) as number;
  return text ? text.toLocaleString('vi-vn', { style: 'currency', currencyDisplay: 'code', currency }) : '';
});