import _ from 'lodash';
import Vue from 'vue';

Vue.filter('toLocaleString', (value: any) => {
  return !value
    ? value
    : value.toLocaleString('en-US', {
      maximumFractionDigits: 8,
      minimumFractionDigits: 2,
    });
});
