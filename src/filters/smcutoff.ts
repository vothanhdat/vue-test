import _ from 'lodash';
import numberUtils from '@/services/numberUtils';
import Vue from 'vue';

Vue.filter('smcutoff', numberUtils.cutoffSmooth);
