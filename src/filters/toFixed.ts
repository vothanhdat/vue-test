import Vue from 'vue';

Vue.filter('toFixed', function (value: any, decimal = 8) {
  return parseFloat(value).toFixed(decimal);
});
