import { ActionContext, Module } from 'vuex';

export interface PAPIStoreState {

}


enum MUTATION_TYPE {
  TEST = 'TEST',
}


const State: PAPIStoreState = {
  [MUTATION_TYPE.TEST](
    store: PAPIStoreState,
    data: any,
  ) {

  },
}

const Mutation = {

}

const Action = {
  login(
    context: ActionContext<PAPIStoreState, RootState>,
    data: any,
  ) {

  },
}

const Getter = {
  isLoggen(state: PAPIStoreState): boolean {
    return false
  },
} as any


export default {
  state: State,
  mutations: Mutation,
  actions: Action,
  getters: Getter,
  namespaced: true,
} as Module<PAPIStoreState, RootState>
