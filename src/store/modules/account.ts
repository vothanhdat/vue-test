import { ActionContext, Module } from 'vuex';

export interface AccountStoreState {

}


enum MUTATION_TYPE {
  TEST = 'TEST',
}


const State: AccountStoreState = {
  [MUTATION_TYPE.TEST](
    store: AccountStoreState,
    data: any,
  ) {

  },
}

const Mutation = {

}

const Action = {
  login(
    context: ActionContext<AccountStoreState, RootState>,
    data: any,
  ) {

  },
}

const Getter = {
  isLoggen(state: AccountStoreState): boolean {
    return false
  },
} as any


export default {
  state: State,
  mutations: Mutation,
  actions: Action,
  getters: Getter,
  namespaced: true,
} as Module<AccountStoreState, RootState>
