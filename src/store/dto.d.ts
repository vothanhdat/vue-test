import { AccountStoreState } from "@/store/modules/account";
import { PAPIStoreState } from "@/store/modules/papi";




declare global {
  type RootState = {
    account: AccountStoreState,
    papi: PAPIStoreState,
  }
}