import Vue from 'vue'
import VueX from 'vuex'
import Account from './modules/account'
import PAPI from './modules/papi'

Vue.use(VueX);

const State = new VueX.Store({
    modules: {
        account: Account,
        papi: PAPI,
    },
})

export default State