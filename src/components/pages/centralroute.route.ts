import Vue from 'vue'
import Home from './home/Home.vue'
import CentralLayout from '../layouts/CentralLayout/CentralLayout.vue'
import { RouteConfig } from 'vue-router';


export default function (): RouteConfig {
  const routers: RouteConfig = {
    path: '/',
    component:CentralLayout,
    children: [
      {
        path: 'home',
        name: 'home',
        component: Home
      },
      {
        path: 'about',
        name: 'about',
        component: () => import(/* webpackChunkName: "about" */ './about/About.vue')
      }
    ]
  }
  return routers
}
