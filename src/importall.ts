
importDirectives();
importFilters()


function importDirectives() {
  const directiveModules = require.context('./directives', true, /\.ts$/);
  const keys = directiveModules.keys();
  keys.forEach((key: string) => {
    directiveModules(key);
  });
}

// importFilters();

function importFilters() {
  const filterModules = require.context('./filters', true, /\.ts$/);
  const keys = filterModules.keys();

  keys.forEach((key: string) => {
    filterModules(key);
  });
}