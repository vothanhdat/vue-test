import Vue from 'vue'
import Router from 'vue-router'
import { RouteConfig } from 'vue-router';

function loadRoutes(): RouteConfig[] {
  const vueRoutes: RouteConfig[] = [];
  const pagesRoutes = require.context(
    '@/components/pages',
    true,
    /\.route\.ts$/
  );
  const keys = pagesRoutes.keys();

  keys.forEach((key: string) => {
    console.log(pagesRoutes(key).default())
    vueRoutes.push(pagesRoutes(key).default())
  });

  return vueRoutes;
}

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: loadRoutes()
})
