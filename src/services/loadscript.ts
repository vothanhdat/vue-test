//@ts-ignore  

function loadScript(src, callback, errorCallback) {
  var scriptElement: HTMLScriptElement,
    loaded: boolean,
    t: HTMLScriptElement;
  loaded = false;
  scriptElement = document.createElement('script');
  scriptElement.type = 'text/javascript';
  scriptElement.src = src;
  scriptElement.onload = (scriptElement as any).onreadystatechange = function () {
    if (!loaded && (!this.readyState || this.readyState == 'complete')) {
      loaded = true;
      callback();
    }
  };
  scriptElement.onerror = errorCallback

  t = document.body.getElementsByTagName('script')[0];
  t.parentNode && t.parentNode.insertBefore(scriptElement, t);
}

const cache: { [k: string]: Promise<void> } = {}

export default async function load(src: string) {
  return cache[src] || (cache[src] = new Promise((resolve, reject) => {
    loadScript(src, resolve, reject)
  }))
}