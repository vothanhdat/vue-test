import _ from 'lodash';
import OrderSymbol from '@/dtos/orderSymbol';

function cutoff(value, precision = 8): string {
  value = _.toString(value);
  let pattern = `(^-?\\d+\\.\\d{1,${precision}})`;
  if (!precision) {
    pattern = `(^-?\\d+)`;
  }

  const reg = new RegExp(pattern);
  const matches = reg.exec(value);

  return matches ? matches[1] : value;
}

function cutoffSmooth(value, precision = 8, removeZero = true): string {
  return removeZero
    ? parseFloat(value).toFixed(precision)
      .replace(/\.([0-9]*[1-9]|0)0+$/, ".$1")
      .replace(/\.0+$/, "")
    : parseFloat(value).toFixed(precision)
}


function cutOffBySymbol(value, symbol: string, orderSymbols: { [index: string]: OrderSymbol }): string {
  let precision = _.get(orderSymbols, `${symbol}.price_limit_decimal`);
  if (_.isUndefined(precision)) {
    precision = maximumDecimal(symbol) as any;
  }

  return cutoff(value, precision as any);
}

function topDigit(value, top = 8): string {
  if (!value) {
    return value;
  }

  value = _.toString(value);
  let digits: string[] = value.split('');
  let finalDigits: string[] = [];
  let counter = 0;
  for (let i = 0; i < digits.length && counter < top; i++) {
    let d = digits[i];
    if (/\d/.test(d)) {
      counter++;
    }

    finalDigits.push(d);
  }

  return finalDigits.join('');
}

export default {
  cutoff,
  topDigit,
  maximumDecimal,
  numberWithComma,
  cutOffBySymbol,
  cutoffSmooth,
};

function maximumDecimal(coinPair: string): number {
  let nDecimal = 8;

  switch (coinPair) {
    case 'GTO_BTC':
      nDecimal = 8;
      break;

    case 'NEO_BTC':
      nDecimal = 8;
      break;

    case 'ETH_BTC':
      nDecimal = 6;
      break;

    case 'ICX_BTC':
      nDecimal = 8;
      break;

    case 'ETH_USDT':
      nDecimal = 3;
      break;

    case 'BTC_USDT':
      nDecimal = 3;
      break;

    case 'NEO_ETH':
      nDecimal = 8;
      break;

    default:
  }

  if (nDecimal == 8 && coinPair.indexOf('_USDT') >= 0) {
    nDecimal = 3;
  }

  if (nDecimal == 8 && coinPair.indexOf('_KNOW') >= 0) {
    nDecimal = 4;
  }

  return nDecimal;
}

function numberWithComma(value: any) {
  if (!value) {
    return value;
  }

  const fractionDigits = value.toString().split('.')[1];
  try {
    value = parseFloat(value);
  } catch (e) {
    //
  }

  let localeString = value.toLocaleString('en-US', {
    maximumFractionDigits: 8,
  });

  localeString = localeString.split('.')[0];
  if (fractionDigits) {
    return localeString + '.' + fractionDigits;
  }

  return localeString;
}
