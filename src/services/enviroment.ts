

const isDevENV = process.env.NODE_ENV === 'development' && window.location.hostname == "localhost";
const isTestENV = process.env.NODE_ENV === 'production' && !isDevENV && (window.location.hostname.indexOf(".surge.") > -1);
const isProdENV = process.env.NODE_ENV === 'production' && !isTestENV && !isDevENV;

const domain = (isDevENV && 'http://localhost:8082')
  || (isTestENV && 'https://kryptono.surge.sh')
  || (isProdENV && 'https://payment.kryptono.exchange/')

const squareKey = (isDevENV && "sandbox-sq0idp-AnJivz2wMP4w1paCixSMEQ")
  || (isTestENV && "sandbox-sq0idp-AnJivz2wMP4w1paCixSMEQ")
  || (isProdENV && 'sq0idp-AnJivz2wMP4w1paCixSMEQ')

const apiRoot : string = (isDevENV && "https://testenv1.kryptono.exchange/k/")
  || (isTestENV && "https://testenv1.kryptono.exchange/k/")
  || (isProdENV && "https://xapi.kryptono.exchange/k/") as string

export default {
  isDevENV,
  isTestENV,
  isProdENV,
  domain,
  squareKey,
  apiRoot,
}